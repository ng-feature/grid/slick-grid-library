var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('combine-js', [], function () {
    return gulp.src([
        'bracket_start.js',
        'lib/jquery-1.7.min.js',
        'lib/jquery-ui-1.8.24.custom.min.js',
        'lib/jquery.event.drag-2.2.js',
        'lib/jquery.event.drop-2.2.js',
        'lib/jquery.jsonp-2.4.min.js',
        'lib/jquery.mousewheel.js',
        'lib/jquery.simulate.js',
        'lib/jquery.sparkline.min.js',
        'slick.core.js',
        'slick.editors.js',
        'slick.formatters.js',
        'plugins/slick.rowselectionmodel.js',
        'slick.grid.js',
        'slick.dataview.js',                        // id -> _idProperty_
        'controls/slick.pager.js',
        'controls/slick.columnpicker.js',
        'bracket_end.js',
        'plugins/slick.columngroup.js',
        'lib/jquery-ui-1.8.16.custom.min.js',
        'plugins/slick.cellrangedecorator.js',
        'plugins/slick.cellrangeselector.js',
        'plugins/slick.cellselectionmodel.js',
        'plugins/slick.autotooltips.js',
        'slick.cellexternalcopymanager.js',          // id -> _idProperty_
        'plugins/slick.autocolumnsize.js'            // id -> _idProperty_
    ])
        .pipe(concat('slickgrid.all.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['combine-js']);
